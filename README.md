Index of links and lists.

# Install as a hugo theme

```
hugo new site your-site
cd your-site
git submodule add https://gitlab.com/sctlib/index-theme.git themes/index-theme
```

> Note: adding the theme as submodule with ssh (`git submodule add
> git@gitlab.com:sctlib/index-theme.git themes/index-theme`) will not
> work with gitlab-ci. It will throw an authentication error, because
> your gitlab (ci) account is not allowed to connect with ssh into the
> gitlab organization where the theme is located.

You should now have:
- `./.gitmodules` file
- `./themes/index-theme/` folder, populated with files and folders.

Then edit the site's `./config.toml`, with adding the line:

```
theme = "index-theme"
```

In your site's root you can now run the development/edition server with:

```
hugo server
```

## Site Params

in `your-site/config.toml`
```
[Params]
withDatabase = false
withNotificationSystem = false
withSearch = false
withMap = false
```

Set to `true` to inject Firebase database and/or a notification system.



## Todo:

- [ ] test if works on joblistberlin, remove now duplicate code there
- [ ] refactor / search, to only display if algolia env keys
- [ ] refactor edit current page components to be more generic on content type
- [ ] be sure map work with any type of content type (that has a title? & clear coordinates)
- [ ] make /tags layout work with /categories too
- [ ] homepage remove jobs and list some, but few, relevent content (featured content type?)
- [ ] refactor all default /sections to handle multiple use pagination?
- [ ] finish take all the default test pages of [hugoBasicExample](https://github.com/gohugoio/hugoBasicExample)

More:
- [ ] extract javascript to specific components; into node modules served with cdn
- [ ] extract css from gohugo theme into a layout system
- [ ] extract web components css into reusable css (or~if import from layout system?)
